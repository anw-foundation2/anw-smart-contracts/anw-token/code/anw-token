const SafeMathLib = artifacts.require('SafeMath');
const ANWToken = artifacts.require('ANWToken');

module.exports = {
    SafeMathLib,
    ANWToken,
};