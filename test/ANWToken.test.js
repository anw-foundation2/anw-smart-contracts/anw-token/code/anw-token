// const { expectRevert } = require('@openzeppelin/test-helpers');
// const expectEvent = require('./helpers/expectEvent');
const { ZERO_ADDRESS } = require('./helpers/constants');
const { setUpUnitTest } = require('./helpers/setUpUnitTest');

const {
    BN,           // Big Number support
    constants,    // Common constants, like the zero address and largest integers
    expectEvent,  // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
    time,
    } = require('@openzeppelin/test-helpers');

contract('ANWToken', function (accounts) {

    const [owner, minter, pauser, recoverer, ...others] = accounts;

    const users = others.slice(0,);
    let token;

    let totalsupply = 1000000000;

    beforeEach(async function () {
        const { instances } = await setUpUnitTest(accounts);
        token = instances.ANWToken;
    });

    describe('roles', function(){
        it('set the contract owner as Minter role', async function () {
            (await token.isMinter(owner, { from: owner })).should.be.equal(true);
        });

        it('set the Minter role', async function () {
            const { logs } = await token.addMinter(minter, { from: owner });

            (await token.isMinter(minter, { from: owner })).should.be.equal(true);

            expectEvent.inLogs(logs, 'MinterAdded', {
                account: minter,
            });
        });

        it('fails to set the Minter role if not called by a Minter', async function () {
            await expectRevert(token.addMinter(users[0], { from: users[0] }), "MinterRole: caller does not have the Minter role");
        });

        it('renounce the Minter role', async function () {
            await token.addMinter(minter, { from: owner });
            const { logs } = await token.renounceMinter({ from: owner });

            (await token.isMinter(owner, { from: owner })).should.be.equal(false);

            expectEvent.inLogs(logs, 'MinterRemoved', {
                account: owner,
            });
        });

        it('fails to renounce the Minter role if there are no other Minter accounts', async function () {
            await expectRevert(token.renounceMinter({ from: owner }), "Roles: there must be at least one account assigned to this role");
        });

        it('set the contract owner as Pauser role', async function () {
            (await token.isPauser(owner, { from: owner })).should.be.equal(true);
        });

        it('set the Pauser role', async function () {
            const { logs } = await token.addPauser(pauser, { from: owner });

            (await token.isPauser(pauser, { from: owner })).should.be.equal(true);

            expectEvent.inLogs(logs, 'PauserAdded', {
                account: pauser,
            });
        });

        it('fails to set the Pauser role if not called by a Pauser', async function () {
            await expectRevert(token.addPauser(users[0], { from: users[0] }), "PauserRole: caller does not have the Pauser role");
        });

        it('renounce the Pauser role', async function () {
            await token.addPauser(pauser, { from: owner });
            const { logs } = await token.renouncePauser({ from: owner });

            (await token.isPauser(owner, { from: owner })).should.be.equal(false);

            expectEvent.inLogs(logs, 'PauserRemoved', {
                account: owner,
            });
        });

        it('fails to renounce the Pauser role if there are no other Pauser accounts', async function () {
            await expectRevert(token.renouncePauser({ from: owner }), "Roles: there must be at least one account assigned to this role");
        });

        it('set the contract owner as Recoverer role', async function () {
            (await token.isRecoverer(owner, { from: owner })).should.be.equal(true);
        });

        it('set the Recoverer role', async function () {
            const { logs } =  await token.addRecoverer(recoverer, { from: owner });

            (await token.isRecoverer(recoverer, { from: owner })).should.be.equal(true);

            expectEvent.inLogs(logs, 'RecovererAdded', {
                account: recoverer,
            });
        });

        it('fails to set the Recoverer role if not called by a Recoverer', async function () {
            await expectRevert(token.addRecoverer(users[0], { from: users[0] }), "RecovererRole: caller does not have the Recoverer role");
        });

        it('renounce the Recoverer role', async function () {
            await token.addRecoverer(recoverer, { from: owner });
            const { logs } = await token.renounceRecoverer({ from: owner });

            (await token.isRecoverer(owner, { from: owner })).should.be.equal(false);

            expectEvent.inLogs(logs, 'RecovererRemoved', {
                account: owner,
            });
        });

        it('fails to renounce the Recoverer role if there are no other Recoverer accounts', async function () {
            await expectRevert(token.renounceRecoverer({ from: owner }), "Roles: there must be at least one account assigned to this role");
        });
    });

    describe('total supply', function () {
        it('returns the total supply of tokens', async function () {
            (await token.totalSupply()).should.be.bignumber.equal(web3.utils.toWei(new BN(totalsupply)));
        });
    });

    describe('balanceOf', function () {
        describe('when the requested account has no tokens', function () {
            it('returns zero', async function () {
                (await token.balanceOf(users[0])).should.be.bignumber.equal(new BN(0));
            });
        });

        describe('when the owner account creates the contract', function () {
            it('returns the total amount of tokens', async function () {
                (await token.balanceOf(owner)).should.be.bignumber.equal(web3.utils.toWei(new BN(totalsupply)));
            });
        });
    });

    describe('transfer', function () {
        describe('when the recipient is not the zero address', function () {
            const to = users[0];

            describe('when the sender does not have enough balance', function () {
                const amount = web3.utils.toWei(new BN(totalsupply+1));

                it('reverts', async function () {
                    await expectRevert(token.transfer(to, amount, { from: owner }), "ERC20: transfer amount exceeds balance");
                });
            });

            describe('when the sender has enough balance', function () {
                const amount = web3.utils.toWei(new BN(totalsupply-100));

                it('transfers the requested amount', async function () {
                    const { logs } = await token.transfer(to, amount, { from: owner });

                    (await token.balanceOf(owner)).should.be.bignumber.equal(web3.utils.toWei(new BN(100)));
                    (await token.balanceOf(to)).should.be.bignumber.equal(amount);

                    expectEvent.inLogs(logs, 'Transfer', {
                        from: owner,
                        to: to,
                        value: amount,
                    });
                });
            });

            describe('when the recipient is the zero address', function () {
                const to = ZERO_ADDRESS;

                it('reverts', async function () {
                    await expectRevert(token.transfer(to, web3.utils.toWei(new BN(100)), { from: owner }), "ERC20: transfer to the zero address");
                });
            });
        });
    });

    describe('approve', function () {
        describe('when the spender is not the zero address', function () {
            const spender = users[0];

            describe('when the owner has enough balance', function () {
                const amount = web3.utils.toWei(new BN(totalsupply-100));

                describe('when there was no approved amount before', function () {
                    it('approves the requested amount', async function () {
                        const { logs } = await token.approve(spender, amount, { from: owner });
                        
                        (await token.allowance(owner, spender)).should.be.bignumber.equal(amount);

                        expectEvent.inLogs(logs, 'Approval', {
                            owner: owner,
                            spender: spender,
                            value: amount,
                        });
                    });
                });

                describe('when the spender had an approved amount', function () {
                    it('approves the requested amount and replaces the previous one', async function () {
                        await token.approve(spender, amount, { from: owner });
                        (await token.allowance(owner, spender)).should.be.bignumber.equal(amount);
                        await token.approve(spender, web3.utils.toWei(new BN(1)), { from: owner });
                        (await token.allowance(owner, spender)).should.be.bignumber.equal(web3.utils.toWei(new BN(1)));
                    });
                });
            });

            describe('when the sender does not have enough balance', function () {
                const amount = web3.utils.toWei(new BN(totalsupply+1));

                describe('when there was no approved amount before', function () {
                    it('approves the requested amount', async function () {
                        const { logs } =  await token.approve(spender, amount, { from: owner });

                        (await token.allowance(owner, spender)).should.be.bignumber.equal(amount);

                        expectEvent.inLogs(logs, 'Approval', {
                            owner: owner,
                            spender: spender,
                            value: amount,
                        });
                    });
                });

                describe('when the spender had an approved amount', function () {
                    it('approves the requested amount and replaces the previous one', async function () {
                        await token.approve(spender, amount, { from: owner });
                        (await token.allowance(owner, spender)).should.be.bignumber.equal(amount);
                        await token.approve(spender, web3.utils.toWei(new BN(1)), { from: owner });
                        (await token.allowance(owner, spender)).should.be.bignumber.equal(web3.utils.toWei(new BN(1)));
                    });
                });
            });
        });

        describe('when the spender is the zero address', function () {
            const spender = ZERO_ADDRESS;
            const amount = web3.utils.toWei(new BN(totalsupply));

            it('reverts', async function () {
                await expectRevert(token.approve(spender, amount, { from: owner }), "ERC20: approve to the zero address");
            });
        });
    });

    describe('transfer from', function () {
      const spender = users[0];

        describe('when the recipient is not the zero address', function () {
            const to = users[1];

            describe('when the spender has enough approved balance', function () {
                beforeEach(async function () {
                    await token.approve(spender, web3.utils.toWei(new BN(totalsupply)), { from: owner });
                });

                describe('when the owner has enough balance', function () {
                    const amount = web3.utils.toWei(new BN(totalsupply));

                    it('transfers the requested amount', async function () {
                        const { logs } = await token.transferFrom(owner, to, amount, { from: spender });

                        (await token.balanceOf(owner)).should.be.bignumber.equal(new BN(0));
                        (await token.balanceOf(to)).should.be.bignumber.equal(amount);

                        expectEvent.inLogs(logs, 'Transfer', {
                            from: owner,
                            to: to,
                            value: amount,
                        });
                    });
                });

                describe('when the owner does not have enough balance', function () {
                    const amount = web3.utils.toWei(new BN(totalsupply));
                    
                    it('reverts', async function () {
                        (await token.balanceOf(owner)).should.be.bignumber.equal(amount);
                        (await token.allowance(owner, spender)).should.be.bignumber.equal(amount);
                        await token.transfer(to, web3.utils.toWei(new BN(100)), {from: owner});
                        await expectRevert(token.transferFrom(owner, to, amount, { from: spender }), "ERC20: transfer amount exceeds balance");
                    });
                });
            });

            describe('when the spender does not have enough approved balance', function () {
                beforeEach(async function () {
                    await token.approve(spender, web3.utils.toWei(new BN(1)), { from: owner });
                });

                describe('when the owner has enough balance', function () {
                    const amount = web3.utils.toWei(new BN(totalsupply));

                    it('reverts', async function () {
                        (await token.allowance(owner, spender)).should.be.bignumber.equal(web3.utils.toWei(new BN(1)));
                        await expectRevert(token.transferFrom(owner, to, amount, { from: spender }), "ERC20: transfer amount exceeds allowance");
                    });
                });

                describe('when the owner does not have enough balance', function () {
                    const amount = web3.utils.toWei(new BN(1));

                    it('reverts', async function () {
                        (await token.balanceOf(owner)).should.be.bignumber.equal(web3.utils.toWei(new BN(totalsupply)));
                        (await token.allowance(owner, spender)).should.be.bignumber.equal(amount);
                        await token.transfer(to, web3.utils.toWei(new BN(totalsupply)), {from: owner});
                        await expectRevert(token.transferFrom(owner, to, amount, { from: spender }), "ERC20: transfer amount exceeds balance");
                    });
                });
            });
        });

        describe('when the recipient is the zero address', function () {
            const amount = web3.utils.toWei(new BN(totalsupply));
            const to = ZERO_ADDRESS;

            beforeEach(async function () {
                await token.approve(spender, amount, { from: owner });
            });

            it('reverts', async function () {
                await expectRevert(token.transferFrom(owner, to, amount, { from: spender }), "ERC20: transfer to the zero address");
            });
        });
    });

    describe('decrease allowance', function () {
        describe('when the spender is not the zero address', function () {
            const spender = users[0];

            describe('when there was no approved amount before', function () {
                const amount = web3.utils.toWei(new BN(1));
                it('reverts', async function () {
                    await expectRevert(token.decreaseAllowance(spender, amount, { from: owner }), "ERC20: decreased allowance below zero");
                });
            });

            describe('when the spender had an approved amount', function () {
                const amount = web3.utils.toWei(new BN(totalsupply));
                beforeEach(async function () {
                    await token.approve(spender, amount, { from: owner });
                });

                it('decreases the spender allowance subtracting the requested amount', async function () {
                    (await token.balanceOf(owner)).should.be.bignumber.equal(web3.utils.toWei(new BN(totalsupply)));
                    (await token.allowance(owner, spender)).should.be.bignumber.equal(amount);
                    const { logs } = await token.decreaseAllowance(spender, amount.sub(web3.utils.toWei(new BN(1))), { from: owner });

                    (await token.allowance(owner, spender)).should.be.bignumber.equal(web3.utils.toWei(new BN(1)));

                    expectEvent.inLogs(logs, 'Approval', {
                        owner: owner,
                        spender: spender,
                        value: web3.utils.toWei(new BN(1)),
                    });
                });

                it('sets the allowance to zero when all allowance is removed', async function () {
                    await token.decreaseAllowance(spender, amount, { from: owner });

                    (await token.allowance(owner, spender)).should.be.bignumber.equal(new BN(0));
                });

                it('reverts when more than the full allowance is removed', async function () {
                    await expectRevert(token.decreaseAllowance(spender, amount.add(web3.utils.toWei(new BN(1))), { from: owner }), "ERC20: decreased allowance below zero");
                });
            });
        });

        describe('when the spender is the zero address', function () {
            const spender = ZERO_ADDRESS;

            it('reverts', async function () {
                await expectRevert(token.decreaseAllowance(spender, new BN(0), { from: owner }), "ERC20: approve to the zero address");
            });
        });
    });

    describe('increase allowance', function () {
        const amount = web3.utils.toWei(new BN(totalsupply));

        describe('when the spender is not the zero address', function () {
            const spender = users[0];

            describe('when the sender has enough balance', function () {

                describe('when there was no approved amount before', function () {
                    it('approves the requested amount', async function () {
                        await token.increaseAllowance(spender, amount, { from: owner });

                        (await token.allowance(owner, spender)).should.be.bignumber.equal(amount);
                    });
                });

                describe('when the spender had an approved amount', function () {
                    beforeEach(async function () {
                        await token.approve(spender, web3.utils.toWei(new BN(1)), { from: owner });
                    });

                    it('increases the spender allowance adding the requested amount', async function () {
                        const { logs } = await token.increaseAllowance(spender, amount, { from: owner });

                        (await token.allowance(owner, spender)).should.be.bignumber.equal(amount.add(web3.utils.toWei(new BN(1))));

                        expectEvent.inLogs(logs, 'Approval', {
                            owner: owner,
                            spender: spender,
                            value: amount.add(web3.utils.toWei(new BN(1))).toString(),
                        });
                    });
                });
            });
        });

        describe('when the spender is the zero address', function () {
            const spender = ZERO_ADDRESS;

            it('reverts', async function () {
                await expectRevert(token.increaseAllowance(spender, new BN(0), { from: owner }), "ERC20: approve to the zero address");
            });
        });
    });

    describe('burn', function () {
        const amount = web3.utils.toWei(new BN(totalsupply));

        describe('when the burner has enough balance', function () {
            it('burns the requested amount', async function () {
                const burnAmount = web3.utils.toWei(new BN(1));
                const { logs } = await token.burn(burnAmount, { from: owner });
                // check balance
                (await token.balanceOf(owner)).should.be.bignumber.equal(amount.sub(burnAmount));
                // check total supply
                (await token.totalSupply()).should.be.bignumber.equal(amount.sub(burnAmount)); 

                // event
                expectEvent.inLogs(logs, 'Transfer', {
                    from: owner,
                    to: ZERO_ADDRESS,
                    value: burnAmount.toString(),
                });
            });
        });

        describe('when the burner does not have enough balance', function () {
            it('reverts', async function () {
                const burnAmount = amount.add(web3.utils.toWei(new BN(1)));
                await expectRevert(token.burn(burnAmount, { from: owner }), "ERC20: burn amount exceeds balance");
            });
        });
    });

    describe('burn from', function () {
        const spender = users[0];
        const amount = web3.utils.toWei(new BN(totalsupply));

        describe('when the owner has enough balance', function () {
            describe('when there is enough approved amount', function () {
                beforeEach(async function () {
                    await token.approve(spender, amount, { from: owner });
                });

                it('burns the requested amount', async function () {
                    const burnAmount = web3.utils.toWei(new BN(1));
                    const { logs } = await token.burnFrom(owner, burnAmount, { from: spender });
                    // check balance
                    (await token.balanceOf(owner)).should.be.bignumber.equal(amount.sub(burnAmount));
                    // check total supply
                    (await token.totalSupply()).should.be.bignumber.equal(amount.sub(burnAmount)); 
                    // event
                    expectEvent.inLogs(logs, 'Transfer', {
                        from: owner,
                        to: ZERO_ADDRESS,
                        value: burnAmount.toString(),
                    });
                });
            });

            describe('when there is not enough approved amount', function () {
                beforeEach(async function () {
                    await token.approve(spender, web3.utils.toWei(new BN(1)), { from: owner });
                });

                it('reverts', async function () {
                    const burnAmount = web3.utils.toWei(new BN(100));
                    await expectRevert(token.burnFrom(owner, burnAmount, { from: spender }), "ERC20: burn amount exceeds allowance");
                });
            });
        });

        describe('when the owner does not have enough balance', function () {
            describe('when there is enough approved amount', function () {
                beforeEach(async function () {
                    await token.approve(spender, amount, { from: owner });
                });
                it('reverts', async function () {
                    const burnAmount = web3.utils.toWei(new BN(totalsupply+1));
                    await expectRevert(token.burnFrom(owner, burnAmount, { from: spender }), "ERC20: burn amount exceeds balance");
                });
            });
        });
    });

    describe('pause', function () {
        describe('when the contract is unpaused', function () {
            const spender = users[0];
            const amount = web3.utils.toWei(new BN(totalsupply-100));

            before(async function () {
                (await token.paused()).should.be.equal(false);
            });

            it('transfers the requested amount', async function () {
                await token.transfer(spender, amount, { from: owner });

                (await token.balanceOf(owner)).should.be.bignumber.equal(web3.utils.toWei(new BN(100)));
                (await token.balanceOf(spender)).should.be.bignumber.equal(amount);
            });

            it('approves the requested amount', async function () {
                await token.approve(spender, web3.utils.toWei(new BN(100)), { from: owner });

                (await token.allowance(owner, spender)).should.be.bignumber.equal(web3.utils.toWei(new BN(100)));
            });

            it('increases allowance for the requested amount', async function () {
                await token.increaseAllowance(spender, web3.utils.toWei(new BN(100)), { from: owner });

                (await token.allowance(owner, spender)).should.be.bignumber.equal(web3.utils.toWei(new BN(100)));
            });

            it('decreases allowance for the requested amount', async function () {
                await token.increaseAllowance(spender, web3.utils.toWei(new BN(100)), { from: owner });
                await token.decreaseAllowance(spender, web3.utils.toWei(new BN(100)), { from: owner });

                (await token.allowance(owner, spender)).should.be.bignumber.equal(web3.utils.toWei(new BN(0)));
            });

            it('transfer from the requested amount', async function () {
                await token.increaseAllowance(spender, web3.utils.toWei(new BN(100)), { from: owner });
                await token.transferFrom(owner, spender, web3.utils.toWei(new BN(100)), { from: spender });

                (await token.balanceOf(owner)).should.be.bignumber.equal(amount);
                (await token.balanceOf(spender)).should.be.bignumber.equal(web3.utils.toWei(new BN(100)));
            });
            
            it('burns the requested amount', async function () {
                await token.burn(web3.utils.toWei(new BN(100)), { from: owner });

                (await token.balanceOf(owner)).should.be.bignumber.equal(amount);
                (await token.totalSupply()).should.be.bignumber.equal(amount);
            });

            it('burn from the requested amount', async function () {
                await token.increaseAllowance(spender, web3.utils.toWei(new BN(100)), { from: owner });
                await token.burnFrom(owner, web3.utils.toWei(new BN(100)), { from: spender });

                (await token.balanceOf(owner)).should.be.bignumber.equal(amount);
                (await token.totalSupply()).should.be.bignumber.equal(amount);
            });
        });

        describe('when the contract is paused', function () {
            const spender = users[0];

            beforeEach(async function () {
                // increase allowance, to test decreaseAllowance
                await token.increaseAllowance(spender, web3.utils.toWei(new BN(100)), { from: owner });
                // pause
                await token.pause({ from: owner });
                (await token.paused()).should.be.equal(true);
            });

            it('reverts transfer', async function () {
                await expectRevert(token.transfer(spender, web3.utils.toWei(new BN(100)), { from: owner }), "Pausable: paused");
            });

            it('reverts approve', async function () {
                await expectRevert(token.approve(spender, web3.utils.toWei(new BN(100)), { from: owner }), "Pausable: paused");
            });

            it('reverts increaseAllowance', async function () {
                await expectRevert(token.increaseAllowance(spender, web3.utils.toWei(new BN(100)), { from: owner }), "Pausable: paused");
            });

            it('reverts decreaseAllowance', async function () {
                await expectRevert(token.decreaseAllowance(spender, web3.utils.toWei(new BN(100)), { from: owner }), "Pausable: paused");
            });

            it('reverts transferFrom', async function () {
                await expectRevert(token.transferFrom(owner, spender, web3.utils.toWei(new BN(100)), { from: spender }), "Pausable: paused");
            });

            it('reverts burn', async function () {
                await expectRevert(token.burn(web3.utils.toWei(new BN(100)), { from: owner }), "Pausable: paused");
            });

            it('reverts burnFrom', async function () {
                await expectRevert(token.burnFrom(owner, web3.utils.toWei(new BN(100)), { from: spender }), "Pausable: paused");
            });
        });
    });

    describe('recover', function () {
        it('recovers the tokens sent to the ANWToken address', async function () {
            const amount = web3.utils.toWei(new BN(totalsupply));
            // send token to contract address
            await token.transfer(token.address, web3.utils.toWei(new BN(100)), { from: owner });
            // check balances
            (await token.balanceOf(owner)).should.be.bignumber.equal(amount.sub(web3.utils.toWei(new BN(100))));
            (await token.balanceOf(token.address)).should.be.bignumber.equal(web3.utils.toWei(new BN(100)));
            // recover
            await token.recoverERC20(token.address, web3.utils.toWei(new BN(100)), { from: owner });
            // check after recovery balances
            (await token.balanceOf(owner)).should.be.bignumber.equal(amount);
            (await token.balanceOf(token.address)).should.be.bignumber.equal(web3.utils.toWei(new BN(0)));
        });
    });

    describe('proposal', function () {
        describe('mintProposal', function(){
            it('reverts when non-minter tries to call', async function () {
                await expectRevert(token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: users[0] }), "MinterRole: caller does not have the Minter role");
            });

            it('reverts when the proposal amount is 0', async function () {
                await expectRevert(token.mintProposal(web3.utils.toWei(new BN(0)), new BN(1800), { from: owner }), "mintProposal: amount is zero");
            });

            it('reverts when the proposal duration is les than 30 min', async function () {
                await expectRevert(token.mintProposal(web3.utils.toWei(new BN(100)), new BN(100), { from: owner }), "mintProposal: duration is less than 30 min");
            });

            it('creates and pushes a new proposal object to the _proposals array', async function () {
                (await token.currentProposalID({ from: owner })).should.be.bignumber.equal(new BN(0));

                const { logs } =  await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                const timestamp = await time.latest();
                const endTime = timestamp.add(new BN(1800));

                expectEvent.inLogs(logs, 'MintProposal', {
                    proposalId: "0",
                    proposalAddress: owner,
                    amount: web3.utils.toWei(new BN(100)).toString(),
                    endTime: endTime.toString(),
                });
                (await token.currentProposalID({ from: owner })).should.be.bignumber.equal(new BN(0));
            });

            it('reverts when trying to call mintProposal before the previous porposal has finished', async function () {
                await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner })
                await expectRevert(token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner }), "mintProposal: proposal already in progress");
            });

            describe('voting', function(){
                const tokenAmount = 10;
                beforeEach(async function () {
                    // send user tokens to vote
                    await token.transfer(users[0], web3.utils.toWei(new BN(tokenAmount)), { from: owner });
                    await token.transfer(users[1], web3.utils.toWei(new BN(tokenAmount)), { from: owner });
                });
                it('reverts when there in no active proposal', async function () {
                    await expectRevert(token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] }), "voting: proposal not active");
                });

                it('reverts when the current proposal has closed voting', async function () {
                    // create proposal
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    // fast forward
                    await time.increase(new BN(1801));
                    await expectRevert(token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] }), "voting: voting period has closed");
                });

                it('reverts when the voting amount is greater than the voters token blance', async function () {
                    // create proposal
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await expectRevert(token.voting(true, web3.utils.toWei(new BN(tokenAmount+1)), { from: users[0] }), "voting: vote amount cannot exceed token balance");
                });

                it('reverts when the user voting amount is currently zero and new vote amount is also 0', async function () {
                    // create proposal
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await expectRevert(token.voting(true, web3.utils.toWei(new BN(0)), { from: users[0] }), "voting: vote amount is zero");
                });

                it('allows any user to vote', async function () {
                    // create proposal
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    //user[0] votes
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[1] });
                    // check vote agree amount
                    const proposalId = await token.currentProposalID();
                    const user0Vote = await token.getVote(users[0], proposalId);
                    user0Vote.vote.should.be.equal(true);
                    user0Vote.amount.should.be.bignumber.equal(web3.utils.toWei(new BN(1)));
                    user0Vote.retrieved.should.be.equal(false);
                    const user1Vote = await token.getVote(users[1], proposalId);
                    user1Vote.vote.should.be.equal(true);
                    user1Vote.amount.should.be.bignumber.equal(web3.utils.toWei(new BN(1)));
                    user1Vote.retrieved.should.be.equal(false);

                });

                it('records the proper agree/disagree amount when user votes agree and revotes to agree', async function () {
                    // create proposal
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });

                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    // proposal agree should be 1, user agree shoudl be 1
                    const proposalId = await token.currentProposalID();

                    const proposalData = await token.getProposal(proposalId);
                    proposalData.agree.should.be.bignumber.equal(web3.utils.toWei(new BN(1)));
                    proposalData.disagree.should.be.bignumber.equal(new BN(0));

                    const user0Vote = await token.getVote(users[0], proposalId);
                    user0Vote.vote.should.be.equal(true);
                    user0Vote.amount.should.be.bignumber.equal(web3.utils.toWei(new BN(1)));

                    // digaree unchanged
                    await token.voting(true, web3.utils.toWei(new BN(2)), { from: users[0] });
                    // proposal agree should be 1, user agree shoudl be 2
                    // digaree unchanged
                    const proposalData2 = await token.getProposal(proposalId);
                    proposalData2.agree.should.be.bignumber.equal(web3.utils.toWei(new BN(2)));
                    proposalData2.disagree.should.be.bignumber.equal(new BN(0));

                    const user0Vote2 = await token.getVote(users[0], proposalId);
                    user0Vote2.vote.should.be.equal(true);
                    user0Vote2.amount.should.be.bignumber.equal(web3.utils.toWei(new BN(2)));
                });

                it('records the proper agree/disagree amount when user votes disagree', async function () {
                    // create proposal
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });

                    await token.voting(false, web3.utils.toWei(new BN(1)), { from: users[0] });

                    // proposal disagree should be 1, user disagree should be 2
                    // agree unchanged
                    const proposalId = await token.currentProposalID();

                    const proposalData = await token.getProposal(proposalId);
                    proposalData.disagree.should.be.bignumber.equal(web3.utils.toWei(new BN(1)));
                    proposalData.agree.should.be.bignumber.equal(new BN(0));

                    const user0Vote = await token.getVote(users[0], proposalId);
                    user0Vote.vote.should.be.equal(false);
                    user0Vote.amount.should.be.bignumber.equal(web3.utils.toWei(new BN(1)));

                    // digaree unchanged
                    await token.voting(false, web3.utils.toWei(new BN(2)), { from: users[0] });
                    // proposal agree should be 1, user agree shoudl be 2
                    // digaree unchanged
                    const proposalData2 = await token.getProposal(proposalId);
                    proposalData2.disagree.should.be.bignumber.equal(web3.utils.toWei(new BN(2)));
                    proposalData2.agree.should.be.bignumber.equal(new BN(0));

                    const user0Vote2 = await token.getVote(users[0], proposalId);
                    user0Vote2.vote.should.be.equal(false);
                    user0Vote2.amount.should.be.bignumber.equal(web3.utils.toWei(new BN(2)));
                });

                it('records the proper agree/disagree amount when user changes their vote from agree to disagree', async function () {
                    // create proposal
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });

                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    // total agree is 1, disagree 0, same for user
                    const proposalId = await token.currentProposalID();

                    const proposalData = await token.getProposal(proposalId);
                    proposalData.agree.should.be.bignumber.equal(web3.utils.toWei(new BN(1)));
                    proposalData.disagree.should.be.bignumber.equal(new BN(0));

                    const user0Vote = await token.getVote(users[0], proposalId);
                    user0Vote.vote.should.be.equal(true);
                    user0Vote.amount.should.be.bignumber.equal(web3.utils.toWei(new BN(1)));

                    await token.voting(false, web3.utils.toWei(new BN(2)), { from: users[0] });
                    // total agree is 0, disagree 1, same for user
                    const proposalData2 = await token.getProposal(proposalId);
                    proposalData2.disagree.should.be.bignumber.equal(web3.utils.toWei(new BN(2)));
                    proposalData2.agree.should.be.bignumber.equal(new BN(0));

                    const user0Vote2 = await token.getVote(users[0], proposalId);
                    user0Vote2.vote.should.be.equal(false);
                    user0Vote2.amount.should.be.bignumber.equal(web3.utils.toWei(new BN(2)));
                    // total agree is 0, digaree is 1, same for user
                });

                it('records the proper agree/disagree amount when user changes their vote from disagree to agree', async function () {
                    // create proposal
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });

                    await token.voting(false, web3.utils.toWei(new BN(1)), { from: users[0] });
                    // total disagree is 1, agree 0, same for user
                    const proposalId = await token.currentProposalID();

                    const proposalData = await token.getProposal(proposalId);
                    proposalData.disagree.should.be.bignumber.equal(web3.utils.toWei(new BN(1)));
                    proposalData.agree.should.be.bignumber.equal(new BN(0));

                    const user0Vote = await token.getVote(users[0], proposalId);
                    user0Vote.vote.should.be.equal(false);
                    user0Vote.amount.should.be.bignumber.equal(web3.utils.toWei(new BN(1)));

                    await token.voting(true, web3.utils.toWei(new BN(2)), { from: users[0] });
                    // total agree is 1, disagree 0, same for user
                    const proposalData2 = await token.getProposal(proposalId);
                    proposalData2.agree.should.be.bignumber.equal(web3.utils.toWei(new BN(2)));
                    proposalData2.disagree.should.be.bignumber.equal(new BN(0));

                    const user0Vote2 = await token.getVote(users[0], proposalId);
                    user0Vote2.vote.should.be.equal(true);
                    user0Vote2.amount.should.be.bignumber.equal(web3.utils.toWei(new BN(2)));
                });

                it('correcty deducts user token to the vote contract when new amount is greater than previous vote amount ', async function () {
                    // create proposal
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });

                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    // user balance tokenAmount-1
                    const userBalance = await token.balanceOf(users[0]);
                    userBalance.should.be.bignumber.equal(web3.utils.toWei(new BN(tokenAmount-1)));

                    await token.voting(true, web3.utils.toWei(new BN(2)), { from: users[0] });
                    // user balance tokenAmount-2
                    const userBalance1 = await token.balanceOf(users[0]);
                    userBalance1.should.be.bignumber.equal(web3.utils.toWei(new BN(tokenAmount-2)));

                });

                it('correctly refunds user token to the user account when new amount is less than previous vote amount', async function () {
                    // create proposal
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });

                    await token.voting(true, web3.utils.toWei(new BN(2)), { from: users[0] });
                    
                    // user balance tokenAmount-2
                    const userBalance = await token.balanceOf(users[0]);
                    userBalance.should.be.bignumber.equal(web3.utils.toWei(new BN(tokenAmount-2)));
                    await token.voting(true, web3.utils.toWei(new BN(0)), { from: users[0] });

                    // user balance tokenAmount
                    const userBalance1 = await token.balanceOf(users[0]);
                    userBalance1.should.be.bignumber.equal(web3.utils.toWei(new BN(tokenAmount)));
                });

                it('properly sets the user new vote status', async function () {
                    // create proposal
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });

                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    const proposalId = await token.currentProposalID();
                    // user vote status true
                    const user0Vote = await token.getVote(users[0], proposalId);
                    user0Vote.vote.should.be.equal(true);
    
                    await token.voting(false, web3.utils.toWei(new BN(1)), { from: users[0] });
                    // user vote status false
                    const user0Vote1 = await token.getVote(users[0], proposalId);
                    user0Vote1.vote.should.be.equal(false);
                });

                it('properly emit a voted event', async function () {
                    // create proposal
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });

                    const { logs } = await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    expectEvent.inLogs(logs, 'Voted', {
                        proposalId: '0',
                        vote: true,
                        amount: web3.utils.toWei(new BN(1)).toString()
                    });
                });

            });

            describe('release proposal', function(){
                const tokenAmount = 10;
                beforeEach(async function () {
                    // send user tokens to vote
                    await token.transfer(users[0], web3.utils.toWei(new BN(tokenAmount)), { from: owner });
                    await token.transfer(users[1], web3.utils.toWei(new BN(tokenAmount)), { from: owner });
                });

                it('reverts when there in no active proposal, case 1 no minting proposal at all', async function () {
                    await expectRevert(token.releaseProposal({ from: users[0] }), "releaseProposal: proposal not active");
                });

                it('reverts when there in no active proposal, case 2 at least one minting proposal has occured', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    // fast forward
                    await time.increase(new BN(1801));
                    // release
                    await token.releaseProposal({ from: users[0] });

                    // try release again
                    await expectRevert(token.releaseProposal({ from: users[0] }), "releaseProposal: proposal not active");
                });

                it('reverts if the voting period has not ended', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    // fast forward
                    await time.increase(new BN(1799));
                    // try release
                    await expectRevert(token.releaseProposal({ from: users[0] }), "releaseProposal: voting period has not ended");
                });//totalsupply 

                it('propely passes when agree is greater than disagree', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    // fast forward
                    await time.increase(new BN(1801));
                    const proposalId = await token.currentProposalID();
                    await token.releaseProposal({ from: users[0] });
                    const proposalData = await token.getProposal(proposalId);
                    proposalData.agree.should.be.bignumber.equal(web3.utils.toWei(new BN(1)));
                    proposalData.disagree.should.be.bignumber.equal(new BN(0));
                    proposalData.isSuccess.should.be.equal(true);
                });

                it('propely fails when disagree is greater than agree', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    await token.voting(false, web3.utils.toWei(new BN(2)), { from: users[1] });
                    // fast forward
                    await time.increase(new BN(1801));
                    const proposalId = await token.currentProposalID();
                    await token.releaseProposal({ from: users[0] });

                    const proposalData = await token.getProposal(proposalId);
                    proposalData.isSuccess.should.be.equal(false);
                    proposalData.agree.should.be.bignumber.equal(web3.utils.toWei(new BN(1)));
                    proposalData.disagree.should.be.bignumber.equal(web3.utils.toWei(new BN(2)));
                });

                it('propely fails when agree is 0', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    // fast forward
                    await time.increase(new BN(1801));
                    const proposalId = await token.currentProposalID();
                    await token.releaseProposal({ from: users[0] });

                    const proposalData = await token.getProposal(proposalId);
                    proposalData.isSuccess.should.be.equal(false);
                    proposalData.agree.should.be.bignumber.equal(web3.utils.toWei(new BN(0)));
                    proposalData.disagree.should.be.bignumber.equal(web3.utils.toWei(new BN(0)));
                });

                it('propely mints new token when passing', async function () {
                    const ownerBalance = await token.balanceOf(owner);
                    ownerBalance.should.be.bignumber.equal(web3.utils.toWei(new BN(totalsupply-20)));
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    // fast forward
                    await time.increase(new BN(1801));
                    const proposalId = await token.currentProposalID();
                    await token.releaseProposal({ from: users[0] });

                    const proposalData = await token.getProposal(proposalId);
                    proposalData.isSuccess.should.be.equal(true);

                    const ownerBalance1 = await token.balanceOf(owner);
                    ownerBalance1.should.be.bignumber.equal(web3.utils.toWei(new BN(totalsupply-20+100)));
                });
                

                it('propely doesnt mint new token when failing', async function () {
                    const ownerBalance = await token.balanceOf(owner);
                    ownerBalance.should.be.bignumber.equal(web3.utils.toWei(new BN(totalsupply-20)));
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    // fast forward
                    await time.increase(new BN(1801));
                    const proposalId = await token.currentProposalID();
                    await token.releaseProposal({ from: users[0] });

                    const proposalData = await token.getProposal(proposalId);
                    proposalData.isSuccess.should.be.equal(false);

                    const ownerBalance1 = await token.balanceOf(owner);
                    ownerBalance1.should.be.bignumber.equal(web3.utils.toWei(new BN(totalsupply-20)));
                });

                it('propely updates isActive', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    // fast forward
                    await time.increase(new BN(1801));
                    const proposalId = await token.currentProposalID();
                    const proposalData = await token.getProposal(proposalId);
                    proposalData.isSuccess.should.be.equal(false);
                    proposalData.isActive.should.be.equal(true);
                    await token.releaseProposal({ from: users[0] });

                    const proposalData1 = await token.getProposal(proposalId);
                    proposalData1.isSuccess.should.be.equal(false);
                    proposalData1.isActive.should.be.equal(false);
                });

                it('propely emits a ReleaseProposal event', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    // fast forward
                    await time.increase(new BN(1801));
                    const { logs} = await token.releaseProposal({ from: users[0] });
                    expectEvent.inLogs(logs, 'ReleaseProposal', {
                        proposalId: '0',
                        proposalAddress: owner,
                        amount: web3.utils.toWei(new BN(100)).toString(),
                        isSuccess: true
                    });
                });

                it('propely updates proposalId', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    const proposalId = await token.currentProposalID();
                    proposalId.should.be.bignumber.equal(new BN(0));
                    // fast forward
                    await time.increase(new BN(1801));
                    await token.releaseProposal({ from: users[0] });
                    const proposalId1 = await token.currentProposalID();
                    proposalId1.should.be.bignumber.equal(new BN(1));
                });

            });

            describe('retrieval', function(){
                const tokenAmount = 10;
                beforeEach(async function () {
                    // send user tokens to vote
                    await token.transfer(users[0], web3.utils.toWei(new BN(tokenAmount)), { from: owner });
                    await token.transfer(users[1], web3.utils.toWei(new BN(tokenAmount)), { from: owner });
                });
                it('reverts when the proposal voting period has not ended', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    const proposalId = await token.currentProposalID();
                    await expectRevert(token.retrieve(proposalId, { from: users[0] }), "retrieve: voting period has not ended");
                });

                it('doesnt return tokens if voting amount was 0 for this proposal', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    await token.voting(true, web3.utils.toWei(new BN(0)), { from: users[0] });
                    const user0Balance = await token.balanceOf(users[0]);
                    
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[1] });
                    // fast forward
                    await time.increase(new BN(1801));
                    const proposalId = await token.currentProposalID();
                    await token.releaseProposal({ from: users[0] });

                    const user0Balance1 = await token.balanceOf(users[0]);

                    await token.retrieve(proposalId, { from: users[0] });
                    user0Balance1.should.be.bignumber.equal(user0Balance);

                });

                it('returns correct amount of tokens when proposal voting has ended and tokens havent been retrieved yet', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    const user0Balance = await token.balanceOf(users[0]);
                    user0Balance.should.be.bignumber.equal(web3.utils.toWei(new BN(tokenAmount-1)));
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[1] });
                    // fast forward
                    await time.increase(new BN(1801));
                    const proposalId = await token.currentProposalID();
                    await token.releaseProposal({ from: users[0] });
                    await token.retrieve(proposalId, { from: users[0] });
                    const user0Balance1 = await token.balanceOf(users[0]);
                    user0Balance1.should.be.bignumber.equal(web3.utils.toWei(new BN(tokenAmount)));
                });

                it('doesnt return tokens if already retieved for this proposal', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });
                    const user0Balance = await token.balanceOf(users[0]);
                    user0Balance.should.be.bignumber.equal(web3.utils.toWei(new BN(tokenAmount-1)));
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[1] });
                    // fast forward
                    await time.increase(new BN(1801));
                    const proposalId = await token.currentProposalID();
                    await token.releaseProposal({ from: users[0] });
                    await token.retrieve(proposalId, { from: users[0] });
                    const user0Balance1 = await token.balanceOf(users[0]);
                    user0Balance1.should.be.bignumber.equal(web3.utils.toWei(new BN(tokenAmount)));
                    await token.retrieve(proposalId, { from: users[0] });
                    const user0Balance2 = await token.balanceOf(users[0]);
                    user0Balance2.should.be.bignumber.equal(web3.utils.toWei(new BN(tokenAmount)));
                });


                it('adjusts the retrieval flag after tokens retrieved', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });

                    // fast forward
                    await time.increase(new BN(1801));
                    const proposalId = await token.currentProposalID();
                    await token.releaseProposal({ from: users[0] });
                    const user0Vote = await token.getVote(users[0], proposalId);
                    user0Vote.retrieved.should.be.equal(false);
                    await token.retrieve(proposalId, { from: users[0] });
                    const user0Vote1 = await token.getVote(users[0], proposalId);
                    user0Vote1.retrieved.should.be.equal(true);

                });

                it('emits a retrieval event', async function () {
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });

                    // fast forward
                    await time.increase(new BN(1801));
                    const proposalId = await token.currentProposalID();
                    await token.releaseProposal({ from: users[0] });
                    const user0Vote = await token.getVote(users[0], proposalId);
                    user0Vote.retrieved.should.be.equal(false);
                    const { logs } = await token.retrieve(proposalId, { from: users[0] });

                    expectEvent.inLogs(logs, 'Retrieved', {
                        proposalId: '0',
                        amount: web3.utils.toWei(new BN(1)).toString()
                    });
                });

                it('retrieves from multiple last proposal rounds with proper amount', async function () {
                    // proposal 0 voted
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });

                    // fast forward
                    await time.increase(new BN(1801));

                    await token.releaseProposal({ from: users[0] });
                    // proposal 1 no-vote
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });

                    // fast forward
                    await time.increase(new BN(1801));

                    await token.releaseProposal({ from: users[0] });
                    // proposal 2 voted
                    await token.mintProposal(web3.utils.toWei(new BN(100)), new BN(1800), { from: owner });
                    await token.voting(true, web3.utils.toWei(new BN(1)), { from: users[0] });

                    // fast forward
                    await time.increase(new BN(1801));

                    await token.releaseProposal({ from: users[0] });
                    const user0Balance = await token.balanceOf(users[0]);
                    user0Balance.should.be.bignumber.equal(web3.utils.toWei(new BN(tokenAmount-2)));
                    await token.retrieveAll({ from: users[0] });
                    const user0Balance1 = await token.balanceOf(users[0]);
                    user0Balance1.should.be.bignumber.equal(web3.utils.toWei(new BN(tokenAmount)));
                });
            });
        });
    });
});