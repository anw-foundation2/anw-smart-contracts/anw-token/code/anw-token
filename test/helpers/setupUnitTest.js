const contractArtifacts = require('./contractArtifacts');
const { BN } = require('./setup');

async function setUpUnitTest (accounts) {
  const [owner, minter, pauser, recoverer, ...others] = accounts;

  const SafeMathLib = await contractArtifacts.SafeMathLib.new();

  const libs = {
    SafeMathLib: SafeMathLib.address,
  };

  await contractArtifacts.ANWToken.link(libs);

  let ANWToken = await contractArtifacts.ANWToken.new({ from: owner });

  const contracts = {ANWToken: ANWToken};
  return { instances: contracts };
}

module.exports = {
  setUpUnitTest,
};